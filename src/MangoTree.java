import java.util.Scanner;

public class MangoTree {
    public static void main(String[] args){
        Scanner scan =new Scanner(System.in);
        System.out.print("Enter no.of rows:");
        int rows= scan.nextInt();
        System.out.print("Enter no.of columns:");
        int cols = scan.nextInt();
        System.out.print("Enter number to check:");
        int numToCheck = scan.nextInt();
        if((numToCheck >=1 && numToCheck < cols) || ((numToCheck -1)% cols ==0) || (numToCheck % cols ==0))
            System.out.println("yes");
        else
            System.out.println("no");
    }
}